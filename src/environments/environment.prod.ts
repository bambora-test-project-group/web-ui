export const environment = {
  production: true,
  apiUrl: 'https://mytest-project-api.herokuapp.com/api',
  successUrl: 'https://bambora-test-ui.azurewebsites.net/success',
  failureUrl: 'https://bambora-test-ui.azurewebsites.net/failure'
};
