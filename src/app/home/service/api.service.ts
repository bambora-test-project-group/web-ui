import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DepositAttributes} from '../DepositAttributes';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private url = environment.apiUrl;

  constructor(private http: HttpClient) {
  }

  public deposit(attributes: DepositAttributes, endUserId: string): Observable<HttpResponse<any>> {
    return this.http.post(this.url + '/trustly/deposit?endUserId=' + endUserId, attributes, {observe: 'response'});
  }
}
