export interface DepositAttributes {
  Country: string;
  Locale: string;
  Currency: string;
  IP: string;
  MobilePhone: string;
  Firstname: string;
  Lastname: string;
  Email: string;
  NationalIdentificationNumber: string;
  SuccessURL: string;
  FailURL: string;
}
