import {Component, OnDestroy, OnInit} from '@angular/core';
import {ApiService} from './service/api.service';
import {environment} from '../../environments/environment';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  private depositSubscription: Subscription;
  private failureUrl = environment.failureUrl;
  private successUrl = environment.successUrl;
  endUserId = '';
  firstName = '';
  lastName = '';
  phoneNumber = '';
  email = '';

  constructor(private apiService: ApiService) {
  }

  onEndUserInputChange(event: any): void {
    this.endUserId = event.target.value;
  }

  onFirstNameInputChange(event: any): void {
    this.firstName = event.target.value;
  }
A
  onLastNameInputChange(event: any): void {
    this.lastName = event.target.value;
  }

  onPhoneNumberInputChange(event: any): void {
    this.phoneNumber = event.target.value;
  }

  onEmailInputChange(event: any): void {
    this.email = event.target.value;
  }

  sendDeposit(): void {
    if (this.endUserId && this.firstName && this.phoneNumber && this.lastName) {
      this.depositSubscription = this.apiService.deposit(
        {
          Country: 'SE',
          Locale: 'sv_SE',
          IP: '121.121.123.123',
          MobilePhone: this.phoneNumber,
          Firstname: this.firstName,
          Lastname: this.lastName,
          Email: this.email,
          NationalIdentificationNumber: '790123-1234',
          SuccessURL: this.successUrl,
          FailURL: this.failureUrl,
          Currency: 'SEK'
        }, this.endUserId).subscribe(resp => {
        window.location.href = resp.body.url;
      });
    }

  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if (this.depositSubscription !== undefined) {
      this.depositSubscription.unsubscribe();
    }
  }
}
