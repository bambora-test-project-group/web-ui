import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {FailureComponent} from './failure/failure.component';
import {HomeComponent} from './home/home.component';
import {SuccessComponent} from './success/success.component';

const appRoutes: Routes = [
  {
    path: 'success',
    component: SuccessComponent
  },
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'failure',
    component: FailureComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false, // <-- debugging purposes only
      }
    )
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {
}
